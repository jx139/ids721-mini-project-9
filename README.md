# Rust Lambda Data Processing Pipeline
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-9/badges/main/pipeline.svg)

This project demonstrates a serverless data processing pipeline built with AWS Lambda and AWS Step Functions, leveraging the power of Rust for Lambda functions. The pipeline performs a series of transformations on input data, showcasing Rust's efficiency and the seamless orchestration capabilities of AWS Step Functions.

## Features

- **UpperCaseTransform Lambda Function:** Converts input text to uppercase.
- **AddTimestamp Lambda Function:** Adds a current timestamp to the data.
- **AWS Step Functions Orchestration:** Orchestrates the Lambda functions in a sequential data processing pipeline.

## Prerequisites

- AWS CLI configured with appropriate access rights.
- Rust installed on your machine.
- `cargo` for Rust package management.
- AWS account and IAM roles set up for Lambda and Step Functions.

## Setup

### Deploying the Lambda Functions

1. **Build the Lambda Function**

   Navigate to the Rust project directory and build the Lambda function for the `x86_64-unknown-linux-musl` target.

   ```sh
   cargo build --release --target x86_64-unknown-linux-musl
   ```

2. **Create the Deployment Package**

   Package the compiled binary as a ZIP file for deployment.

   ```sh
   cp ./target/x86_64-unknown-linux-musl/release/bootstrap .
   zip lambda.zip bootstrap
   ```

3. **Deploy the Lambda Function**

   Use the AWS CLI to deploy the function. Repeat for each Lambda function in the pipeline.

   ```sh
   aws lambda create-function --function-name rustDataProcessing \
   --handler doesnt.matter \
   --zip-file fileb://./lambda.zip \
   --runtime provided.al2 \
   --role arn:aws:iam::YOUR_ACCOUNT_ID:role/YOUR_LAMBDA_EXECUTION_ROLE
   ```
![lambda](./pic/lambda.png)
![lambdaiam](./pic/lambdaiam.png)
![function](./pic/function.png)

### Setting Up the Step Functions State Machine

1. **Define the State Machine**

   Create a JSON file (`state_machine.json`) defining the state machine. Refer to the AWS Step Functions documentation for the syntax.

2. **Create the State Machine**

   ```sh
   aws stepfunctions create-state-machine --name "RustDataProcessingPipeline" \
   --definition file://state_machine.json \
   --role-arn "arn:aws:iam::YOUR_ACCOUNT_ID:role/YOUR_STEP_FUNCTIONS_ROLE"
   ```
![step](./pic/step.png)
![stepiam](./pic/stepiam.png)

## Usage

Start an execution of your state machine either via the AWS Management Console or using the AWS CLI:

```sh
aws stepfunctions start-execution --state-machine-arn <state-machine-arn> --input '{
  "name": "john doe",
  "email": "john.doe@example.com",
  "comments": "hello, world!"
}'
```

1. **After Stage 1 (UpperCaseTransform):**

   ```json
   {
     "name": "JOHN DOE",
     "email": "JOHN.DOE@EXAMPLE.COM",
     "comments": "HELLO, WORLD!"
   }
   ```

2. **After Stage 2 (AddTimestamp):**

   ```json
   {
     "name": "JOHN DOE",
     "email": "JOHN.DOE@EXAMPLE.COM",
     "comments": "HELLO, WORLD!",
     "timestamp": "2024-04-10T12:34:56Z"
   }
   ```

## Development

For local development and testing of the Rust Lambda functions, you may use the [AWS SAM CLI](https://aws.amazon.com/serverless/sam/) or [localstack](https://localstack.cloud/) to emulate AWS services on your local machine.