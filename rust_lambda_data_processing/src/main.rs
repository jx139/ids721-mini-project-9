use chrono::prelude::*;
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use serde_json::Value; // Import chrono to work with dates and times

#[derive(Deserialize, Serialize)]
struct CustomEvent {
    message: String,
}

#[derive(Serialize)]
struct CustomOutput {
    transformed_message: String,
    timestamp: String, // Add a field for the timestamp
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: Value, _: Context) -> Result<CustomOutput, Error> {
    let event: CustomEvent = serde_json::from_value(event)?;

    // Get the current UTC time and format it as an ISO 8601 string
    let now = Utc::now();
    let timestamp = now.to_rfc3339();

    Ok(CustomOutput {
        transformed_message: event.message.to_uppercase(),
        timestamp, // Include the timestamp in the output
    })
}
